#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np              # Berechnungen
import matplotlib.pyplot as plt # Darstellung
import re                       # RegEx

# Matrix für Spiegelung
sym_matrices = {
        'mx': [[-1, 0, 0],
               [0, 1, 0],
               [0, 0, 1]],
        'my': [[1, 0, 0],
               [0, -1, 0],
               [0, 0, 1]],
        'mz': [[1, 0, 0],
               [0, 1, 0],
               [0, 0, -1]]
}


def glide_plane(g):
    # Darstellung der Gleitspiegelebene
    # Punktenetz erzeugen
    x = np.linspace(-4, 12, 11)
    y = np.linspace(-4, 12, 11)
    z = np.linspace(-4, 12, 11)
    # Je nach Lage eine Koordinate 0 setzten
    if g == 'a':
        x, y = np.meshgrid(x, y)
        z = np.zeros_like(x)
    if g == 'b':
        y, z = np.meshgrid(y, z)
        x = np.zeros_like(y)
    if g == 'c':
        y, z = np.meshgrid(y, z)
        x = np.zeros_like(y)
    if g == 'n':
        y, z = np.meshgrid(y, z)
        x = np.zeros_like(y)  
    # Gleitspiegelebene anzeigen
    ax.plot_surface(x, y, z, alpha=0.5, label='{}$\perp a$'.format(g))

def points(g, pxyz): 
    # Berechnung der Punktkoordinaten abhängig von g
    # Transformationsmatrix für Spiegelung + Translation
    # Anpassung an Koordinatensystem; deshalb > 1
    # 's_ma' für Gerade auf Spiegelebene
    if g == 'a':
        m    = sym_matrices.get(str('mz'))
        t_ax = [1/2*10,0,0]
        s_ma = [1,1,0]
    if g == 'b':
        m    = sym_matrices.get(str('mx'))
        t_ax = [0,1/2*10,0]
        s_ma = [0,1,1]
    if g == 'c':
        m    = sym_matrices.get(str('mx'))
        t_ax = [0,0,1/2*10]
        s_ma = [0,1,1]
    if g == 'n':
        m    = sym_matrices.get(
        str('mx'))
        t_ax = [0,1/2*10,1/2*10]
        s_ma = [0,1,1]
    
    # Punkt Spiegelung + 1/2 Translation
    pxyz1 = np.dot(pxyz, m)
    pxyz1 = np.add(pxyz1, t_ax)
    
    # Punkt Spiegelung + 1/2 Translation 
    # bzw. 1 von pxyz ausgegehnd
    pxyz2 = np.dot(pxyz1, m)
    pxyz2 = np.add(pxyz2, t_ax)
    
    # Punkt dartellen
    x,y,z = pxyz
    ax.scatter(x,y,z,label='Startkoordinaten')
    # Gerade vom Punkt auf die Spiegelebene zeichnen
    x1,y1,z1 = pxyz * s_ma
    ax.plot([x,x1],[y,y1],[z,z1],'--',color='black')
    
    # Punkt dartellen
    x,y,z = pxyz1
    ax.scatter(x,y,z,label='½ Translation')
    # Gerade vom Punkt auf die Spiegelebene zeichnen
    x1,y1,z1 = pxyz1 * s_ma
    ax.plot([x,x1],[y,y1],[z,z1],'--',color='black')
    
    # Punkt dartellen
    x,y,z = pxyz2
    ax.scatter(x,y,z,label='Vollst. Translation (1)')
    # Gerade vom Punkt auf die Spiegelebene zeichnen
    x1,y1,z1 = pxyz2 * s_ma
    ax.plot([x,x1],[y,y1],[z,z1],'--',color='black')

g = input('Spiegelebene eingeben (a, b, c, or n) oder "Enter" für c: ')
pattern = r'^[abcn]$'

if not re.match(pattern, g):
    print("Falsche oder keine Eingabe. Wähle c.")
    g = 'c'
    
# Punktkoordinaten
pxyz = np.array([-2,-2,2])


# Plot
fig = plt.figure()
ax  = fig.add_subplot(projection='3d')

# Gleitpspiegelebene und Punkte darstellen
glide_plane(g)
points(g,pxyz)

ax.set_xlabel('$a$')
ax.set_ylabel('$b$')
ax.set_zlabel('$c$')
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
plt.gca().set_zticklabels([])
plt.legend(loc='upper left',ncols=2)
plt.show()
